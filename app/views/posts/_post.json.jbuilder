json.extract! post, :id, :title, :content
json.url post_url(post, format: :json)